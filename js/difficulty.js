function difficulty(clicked_id){
  if (clicked_id === "1") {
    document.getElementById("0").removeAttribute("style");
    document.getElementById("6").removeAttribute("style");
    document.getElementById("16").removeAttribute("style");
    document.getElementById("26").removeAttribute("style");
    document.getElementById("reset").removeAttribute("style");
    document.getElementById("score").removeAttribute("style");
    document.getElementById("1").disabled = true;
    document.getElementById("2").disabled = true;
    level = 'easy';
  }
  if (clicked_id === "2") {
    document.getElementById("0").removeAttribute("style");
    document.getElementById("6").removeAttribute("style");
    document.getElementById("16").removeAttribute("style");
    document.getElementById("26").removeAttribute("style");
    document.getElementById("52").removeAttribute("style");
    document.getElementById("reset").removeAttribute("style");
    document.getElementById("score").removeAttribute("style");
    document.getElementById("1").disabled = true;
    document.getElementById("2").disabled = true;
    level = 'hard';
  }

}