let score = [];


// amount of cards on the field
function reply_click(clicked_id){
    if (clicked_id === "6") {
        start(6);
      }
    if (clicked_id === "16") {
        start(16);
      }
      if (clicked_id === "26") { 
        start(26);
      }
      if (clicked_id === "52") { 
        start(52);
      }
  }

function start(a){

  console.log(level);
  document.getElementById("1").disabled = true;
  document.getElementById("2").disabled = true;
  document.getElementById("6").disabled = true;
  document.getElementById("16").disabled = true;
  document.getElementById("26").disabled = true;
  document.getElementById("52").disabled = true;

  

//1 array with cards
let cardsArray = [
  {
    name: 'aDr',
    img: 'img/a.jpg',
  },
  {
    name: 'aHr',
    img: 'img/aH.jpg',
  },
  {
    name: 'aSb',
    img: 'img/aS.jpg',
  },
  {
    name: 'aCb',
    img: 'img/aC.jpg',
  }, 
  {
    name: 'kDr',
    img: 'img/kD.jpg',
  },
  {
    name: 'kHr',
    img: 'img/kH.jpg',
  },
  {
    name: 'kSb',
    img: 'img/kS.jpg',
  },
  {
    name: 'kCb',
    img: 'img/kC.jpg',
  }, 
  {
    name: 'qDr',
    img: 'img/qD.jpg',
  },
  {
    name: 'qHr',
    img: 'img/qH.jpg',
  },
  {
    name: 'qSb',
    img: 'img/qS.jpg',
  },
  {
    name: 'qCb',
    img: 'img/qC.jpg',
  },  
  {
    name: 'jDr',
    img: 'img/jD.jpg',
  },
  {
    name: 'jHr',
    img: 'img/jH.jpg',
  },
  {
    name: 'jSb',
    img: 'img/jS.jpg',
  },
  {
    name: 'jCb',
    img: 'img/jC.jpg',
  }, 
  {
    name: '1Dr',
    img: 'img/10D.jpg',
  },
  {
    name: '1Hr',
    img: 'img/10H.jpg',
  },
  {
    name: '1Sb',
    img: 'img/10S.jpg',
  },
  {
    name: '1Cb',
    img: 'img/10C.jpg',
  }, 
  {
    name: '9Dr',
    img: 'img/9D.jpg',
  },
  {
    name: '9Hr',
    img: 'img/9H.jpg',
  },
  {
    name: '9Sb',
    img: 'img/9S.jpg',
  },
  {
    name: '9Cb',
    img: 'img/9C.jpg',
  }, 
  {
    name: '8Dr',
    img: 'img/8D.jpg',
  },
  {
    name: '8Hr',
    img: 'img/8H.jpg',
  },
  {
    name: '8Sb',
    img: 'img/8S.jpg',
  },
  {
    name: '8Cb',
    img: 'img/8C.jpg',
  }, 
  {
    name: '7Dr',
    img: 'img/7D.jpg',
  },
  {
    name: '7Hr',
    img: 'img/7H.jpg',
  },
  {
    name: '7Sb',
    img: 'img/7S.jpg',
  },
  {
    name: '7Cb',
    img: 'img/7C.jpg',
  }, 
  {
    name: '6Dr',
    img: 'img/6D.jpg',
  },
  {
    name: '6Hr',
    img: 'img/6H.jpg',
  },
  {
    name: '6Sb',
    img: 'img/6S.jpg',
  },
  {
    name: '6Cb',
    img: 'img/6C.jpg',
  }, 
  {
    name: '5Dr',
    img: 'img/5D.jpg',
  },
  {
    name: '5Hr',
    img: 'img/5H.jpg',
  },
  {
    name: '5Sb',
    img: 'img/5S.jpg',
  },
  {
    name: '5Cb',
    img: 'img/5C.jpg',
  }, 
  {
    name: '4Dr',
    img: 'img/4D.jpg',
  },
  {
    name: '4Hr',
    img: 'img/4H.jpg',
  },
  {
    name: '4Sb',
    img: 'img/4S.jpg',
  },
  {
    name: '4Cb',
    img: 'img/4C1.jpg',
  }, 
  {
    name: '3Dr',
    img: 'img/3D.jpg',
  },
  {
    name: '3Hr',
    img: 'img/3H.jpg',
  },
  {
    name: '3Sb',
    img: 'img/3S.jpg',
  },
  {
    name: '3Cb',
    img: 'img/3C.jpg',
  }, 
  {
    name: '2Dr',
    img: 'img/2D.jpg',
  },
  {
    name: '2Hr',
    img: 'img/2H.jpg',
  },
  {
    name: '2Sb',
    img: 'img/2S.jpg',
  },
  {
    name: '2Cb',
    img: 'img/2C.jpg',
  }, 
]


// set length to remove elements in array
let arrayX;

arrayX = a;

cardsArray.length = arrayX; 

const gameGrid = cardsArray

//8 Duplicate array to create a match


 //9Randomize game grid
.sort(() => 0.5 - Math.random());

let firstGuess; //for matching value&suit&color
let secondGuess;
let thirdLetterFirstChoice; //for matching value&color
let thirdLetterSecondChoice;
let firstLetterFirstChoice; //for matching value
let firstLetterSecondChoice;

let count = 0; //12 for counting only two selected cards
let count2 = 0; // counting for end of the game
let previousTarget = null; //for not matching same selected element twice
let delay = 1200; //delay for the game
let count1 = 0; //counting wrong attemps

//2 grab the div with an id 
let game = document.getElementById('game');

//3 create a section in html
let grid = document.createElement('section');

//4  set class=grid
grid.setAttribute('class', 'grid');

//5 append (добавление) grid to the game
game.appendChild(grid);

//6 each item in the cardsArray array
gameGrid.forEach(item => {
 
  //making object with two property name and img
  let { name, img } = item;

  // create div
  let card = document.createElement('div');

  // create class=card to a card 
  card.classList.add('card');

  //7 set date-name attribute to card with data-name=name
  card.dataset.name = name;

  // create div for front card and adding them
  let front = document.createElement('div');
  front.classList.add('front');

  //same for back
  let back = document.createElement('div');
  back.classList.add('back');

  // apply the background image of the div to the cardsArray image
  back.style.backgroundImage = `url(${img})`;

  // append card to the grid and front and back to the card
  grid.appendChild(card);
  card.appendChild(front);
  card.appendChild(back);
  
});

// 13 function for selected elements
let match = () => {
  let selected = document.querySelectorAll('.selected');

  //add the match class.
  selected.forEach(card => {
    card.classList.add('match');
    
  });
};

//reseting all
let reset = () => {
  
  firstGuess = '';
  secondGuess = '';
  count = 0;
  previousTarget = null;

  let selected = document.querySelectorAll('.selected');
  selected.forEach(card => {
    card.classList.remove('selected');
  });

  //call finish function when all cards are guessed
  if (count2 === a / 2) {
    finish ();
    
  }
};

//10 Add event listener to grid
grid.addEventListener('click', event => {

  // clicked item event target
  let clicked = event.target;

  //only select divs inside the grid 
  if (
    clicked.nodeName === 'SECTION' ||
    clicked === previousTarget ||
    clicked.parentNode.classList.contains('selected') ||
    clicked.parentNode.classList.contains('match')
  ) {
    return;
  }

  //checking number of moves
  if (count < 2) {
    count +=1;
    if (count === 1) {
      //first guess
      firstGuess = clicked.parentNode.dataset.name;
      firstLetterFirstChoice = firstGuess.charAt(0);
      thirdLetterFirstChoice = firstGuess.charAt(2);
      
      clicked.parentNode.classList.add('selected');

    
    } else {
      //second guess
      secondGuess = clicked.parentNode.dataset.name;
      firstLetterSecondChoice = secondGuess.charAt(0);
      thirdLetterSecondChoice = secondGuess.charAt(2);
      
      //add selected class
      clicked.parentNode.classList.add('selected');
    }
    
    

    //if both guesses === same color&value
    if (level === 'hard') {
      if (firstGuess && secondGuess) {
        if (firstLetterFirstChoice === firstLetterSecondChoice && thirdLetterFirstChoice === thirdLetterSecondChoice) {
          count2 += 1;
  
          //run the match function
          setTimeout(match, delay);
        }
        else {
          count1 += 1;
          document.getElementById("notEqual").innerHTML = "Wrong attempts: " + count1;
        }
        //run the reset function
        setTimeout(reset, delay);
      }
      }

    //if both guesses === same value
    else if (level === 'easy') {
      if (firstGuess && secondGuess) {
        if (firstLetterFirstChoice === firstLetterSecondChoice) {
          count2 += 1;
  
          //run the match function
          setTimeout(match, delay);
        }
        else if(firstLetterFirstChoice !== firstLetterSecondChoice){
          count1 += 1;
          document.getElementById("notEqual").innerHTML = "Wrong attempts: " + count1;
        }
        //run the reset function
        setTimeout(reset, delay);
      }
      }
    
    ////set previous target to clicked
    previousTarget = clicked;
    
    
  }

});

function finish (){

  clearTimeout(t); //stop time

  gameTime = document.getElementById("cas").textContent; //getting time played

  let finalCount = 100 - count1;
  
  let result = {Time:gameTime, Score:finalCount};

  score.push(result);

  let items = '<ul>';
  let p = 0; //count number of game


  score.forEach((el) => {
    p +=1;
    items += '<li>' + "Game " + p + " - " + "Time played: " + el.Time + ", " + "Score: " + el.Score + '</li>'
  });

  items += '</ul>';

  document.getElementById('scoreModal').innerHTML = items;

  $("#myModal").modal();
  
}

}

function resetGame(){
  clearTimeout(t);
  time1.textContent = "00:00:00";
  seconds = 0; minutes = 0; hours = 0;
  document.getElementById("game").innerHTML = "";
  document.getElementById("0").style.visibility = 'hidden'
  document.getElementById("6").style.visibility = 'hidden'
  document.getElementById("16").style.visibility = 'hidden'
  document.getElementById("26").style.visibility = 'hidden'
  document.getElementById("52").style.visibility = 'hidden'
  document.getElementById("reset").style.visibility = 'hidden'
  document.getElementById("score").style.visibility = 'hidden'
  document.getElementById("1").disabled = false;
  document.getElementById("2").disabled = false;
  document.getElementById("6").disabled = false;
  document.getElementById("16").disabled = false;
  document.getElementById("26").disabled = false;
  document.getElementById("52").disabled = false;
 
  document.getElementById("notEqual").innerHTML = "Wrong attempts: 0";
  } 

  




// used material:
// https://www.taniarascia.com/how-to-create-a-memory-game-super-mario-with-plain-javascript/ 
// https://jsfiddle.net/Daniel_Hug/pvk6p/


//3 создание сетки. document.createElement() создание элемента

//4 grid.setAttribute('class', 'grid') Добавляем class='grid' в grid т.е. в секцию. Настройки grid в css

//5 game.appendChild(grid) добавляет grid в html с id game

//6 gameGrid.forEach(item => цикл который обрабатывает каждый элемент в массиве

//7 позволяет из javascript получить доступ в режиме чтения и записи к атрибутам data-*, установленным для html-элемента.








